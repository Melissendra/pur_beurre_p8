from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q, Count
from django.shortcuts import get_object_or_404, redirect
from .models import Product, Favorite


class SearchView(ListView):
    """ Return the search input and send the request to our db."""
    model = Product
    template_name = 'products/search_products.html'
    paginate_by = 9

    def get_queryset(self):
        product_query = self.request.GET.get("product_search")
        return Product.objects\
            .filter(Q(product_name__icontains=product_query) |
                    Q(categories__category_name__icontains=product_query))\
            .distinct()\
            .order_by('product_name')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = self.request.GET.get("product_search")
        return context


class ResultView(ListView):
    """
        Return substitutes with same category but better or at list
        same nutrition grades.
    """
    context_object_name = "products_list"
    template_name = 'products/results.html'
    paginate_by = 6

    def get_queryset(self, common_cat=3):
        self.product = get_object_or_404(Product, pk=self.kwargs['pk'])
        cat = self.product.categories.all()
        return Product.objects\
            .filter(categories__in=cat,
                    nutrition_grades__lte=self.product.nutrition_grades)\
            .annotate(similar=Count('categories'))\
            .filter(similar__gte=common_cat)\
            .order_by('nutrition_grades')[:9]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = self.product.categories
        context['product'] = self.product
        context['image'] = self.product.image_url
        return context


class ProductDetail(DetailView):
    """"return the substitute's details"""
    model = Product
    template_name = 'products/details.html'
    context_object_name = 'substitute'


class MyProductView(LoginRequiredMixin, ListView):
    """ Class that allow the display of the user's saved products"""
    template_name = 'products/my_products.html'
    paginate_by = 6

    def get_queryset(self):
        return Favorite.objects.filter(
            user_id=self.request.user.id
        ).order_by('-id')

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = "Mes produits favoris"
        return context


@login_required
def substitute(request, product_id, substitute_id):
    Favorite.objects.create(
        product_id=product_id,
        substitute_id=substitute_id,
        user_id=request.user.id
    )
    return redirect('products:myproducts')
