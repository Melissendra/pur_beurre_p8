from django.urls import path
from .views import SearchView, ResultView, ProductDetail, MyProductView, substitute

app_name = 'products'
urlpatterns = [
    path('results/<int:pk>', ResultView.as_view(), name='results'),
    path('search/', SearchView.as_view(), name="search"),
    path('<int:pk>/details/', ProductDetail.as_view(), name='details'),
    path('myproducts/', MyProductView.as_view(), name='myproducts'),
    path('substitute/<int:product_id>/<int:substitute_id>', substitute, name='substitute'),
]
