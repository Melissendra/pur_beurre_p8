from django.urls import path
from .views import SignUpView, AccountView
from django.contrib.auth import views


urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    path('account/', AccountView.as_view(), name='account'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('logout/', views.LogoutView.as_view(), name='logout')
]
