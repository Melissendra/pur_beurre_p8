from django.test import TestCase
from django.contrib.auth import get_user_model


class CustomUserManagerTest(TestCase):
    """ Test of the user personalization and it's method """
    def test_create_user(self):
        """ Test the creation of a simple user """
        User = get_user_model()
        user = User.objects.create_user(
            email= 'test@test.com',
            password='test1234',
        )

        self.assertEqual(user.email, 'test@test.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)

    def test_create_superuser(self):
        """ Test of the creation of an administrator """
        User = get_user_model()
        admin_user = User.objects.create_superuser(
            email = 'admin@test.com',
            password = 'monsupermodp2310',
        )
        
        self.assertEqual(admin_user.email, 'admin@test.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
