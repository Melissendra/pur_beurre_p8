from django.test import TestCase
from django.urls import reverse


class SignupPageTest(TestCase):
    """ To test the loading of the signup page """

    def test_signup_status_code(self):
        response = self.client.get('/users/signup/')
        self.assertEqual(response.status_code, 200)

    def setUp(self):
        """ Setup method to load the signup page """
        url = reverse('signup')
        self.response= self.client.get(url)

    def test_signup_template(self):
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'users/signup.html')
        self.assertContains(self.response, 'Sign Up')
        self.assertNotContains(self.response, 'Wrong page.')



