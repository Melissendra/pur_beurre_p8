from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import UserProfile
from .forms import SignupForm


class CustomUserAdmin(UserAdmin):
    # form = LoginForm
    add_form = SignupForm
    model = UserProfile
    list_display = ('first_name', 'last_name', 'email',)


admin.site.register(UserProfile)
