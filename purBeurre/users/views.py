from django.views.generic import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy

from .forms import SignupForm


class SignUpView(CreateView):
    form_class = SignupForm
    success_url = reverse_lazy('login')
    template_name = 'users/signup.html'


class AccountView(LoginRequiredMixin, CreateView):
    template_name = 'users/account.html'

    def get(self, request, *args, **kwargs):
        fullname = ' '.join([request.user.first_name, request.user.last_name])
        context = {'fullname': fullname}
        return render(request, self.template_name, context)
