# from django import forms
from django.contrib.auth.forms import UserChangeForm, \
    UserCreationForm
from .models import UserProfile


class SignupForm(UserCreationForm):
    """
        Forms to create a user, with no privileges,
        from the given email and password
    """

    class Meta:
        model = UserProfile
        fields = (
            'last_name',
            'first_name',
            'email',
            )


class SignupChange(UserChangeForm):
    class Meta:
        model = UserProfile
        fields = (
            'last_name',
            'first_name',
            'email',
        )
